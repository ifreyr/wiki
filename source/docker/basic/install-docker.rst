.. _install-docker:

=================
Docker 安装教程
=================

本文以 CentOS 7 下安装 Docker 为例，其他系统安装参考： `Docker Community Edition`_ 。

.. _Docker Community Edition: https://store.docker.com/search?type=edition&offering=community


安装 Docker CE
==================

如果已经安装了旧版本，使用如下命令卸载：

::

    $ sudo yum remove docker docker-common docker-selinux docker-engine


然后安装依赖：

::

    $ sudo yum install -y yum-utils device-mapper-persistent-data lvm2

添加 Docker yum 源：

::

    $ sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

使用 yum 安装：

::

    $ sudo yum install docker-ce


查看版本，确认安装成功：

::

    $ docker version
    Client:
     Version:	18.03.0-ce
     API version:	1.37
     Go version:	go1.9.4
     Git commit:	0520e24
     Built:	Wed Mar 21 23:09:15 2018
     OS/Arch:	linux/amd64
     Experimental:	false
     Orchestrator:	swarm

    Server:
     Engine:
      Version:	18.03.0-ce
      API version:	1.37 (minimum version 1.12)
      Go version:	go1.9.4
      Git commit:	0520e24
      Built:	Wed Mar 21 23:13:03 2018
      OS/Arch:	linux/amd64
      Experimental:	false



启动 Docker
==============

::

    $ sudo systemctl start docker

关闭 Docker
==============

::

    $ sudo systemctl stop docker

开机启动
==============

::

    $ sudo systemctl enable docker

镜像加速器
==============

使用加速器可以提升获取 Docker 官方镜像的速度：

.. tip::

    - 中科大源：http://mirrors.ustc.edu.cn/help/dockerhub.html
    - 阿里云：https://专属镜像加速器.mirror.aliyuncs.com

更新 ``/etc/docker/daemon.json`` 为：

::

    {
        "registry-mirrors": ["https://docker.mirrors.ustc.edu.cn/"]
    }

然后重启服务：

::

    $ sudo systemctl daemon-reload
    $ sudo systemctl restart docker










