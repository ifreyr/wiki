.. _dockerfile-tutorial:

========================
Dockerfile 入门指南
========================



镜像和容器
================

.. image:: /images/image-and-container.jpg

.. note::

    **镜像** （Image）就是一堆只读层（read-only layer）的统一视角。

.. image:: /images/docker-image.jpg

.. note::

    **容器** （container）的定义和镜像（image）几乎一模一样，也是一堆层的统一视角，**唯一区别在于容器的最上面那一层是可读可写的** 。

.. image:: /images/docker-container.jpg

详细的说明以及 Docker 命令的理解参考：`Docker 容器和镜像的区别`_ 。

.. _Docker 容器和镜像的区别: https://www.cnblogs.com/bethal/p/5942369.html

第一个 Dockerfile
=====================

创建一个名为 web 的目录并在里面创建初始的 Dockerfile，这个目录就是我们的构建环境（build environment），Docker 称此环境为上下文（context）。Docker 会在构建镜像时将上下文和其中的文件及目录上传到 Docker 守护进程，这样守护进程就能直接访问用户想在镜像中存储的任何代码、文件或数据。

::

    # Version: 0.0.1
    FROM ubuntu:14.04
    MAINTAINER datascience "datascience@example.com"
    RUN apt-get update && apt-get install -y nginx
    RUN echo 'Hi, I am in your container' > /usr/share/nginx/html/index.html
    EXPOSE 80

Dockerfile 由一系列指令和参数组成，指令会按顺序从上到下执行（所以需要合理安排指令的顺序）。

每条指令都会创建一个新的镜像层并提交（参考本文开始图片），大致执行流程：

- Docker 从基础镜像运行一个容器
- 执行一条指令，并对容器做出修改
- 提交新的镜像层
- 再基于刚提交的镜像运行一个新的容器
- 执行下一条指令，直到所有指令执行完毕

可以看出，如果 Dockerfile 由于某些原因（如某条指令失败）没有正常结束，也能得到一个可以使用的镜像。这对调试非常有帮助。

FROM
----------

每个 Dockerfile 的第一条指令必须是 FROM，用于指定一个已经存在的镜像（基础镜像）。

MAINTAINER
-----------------

指定本镜像的作者和邮件。

RUN
----------

每条 RUN 指令都会创建一个新的镜像层。默认 RUN 指令会在 shell 里使用 ``/bin/sh -c`` 来执行。

EXPOSE
------------

指定 Docker 该容器内应用将会使用容器的端口。可以使用多个 EXPOSE 来向外公开多个端口。

使用 Dockerfile 构建新镜像
=================================

在上下文环境中（这里即 web 目录内）执行 ``docker build`` ：

::

    $ docker build -t "datascience/web" .

其中，``-t`` 选项指定了新镜像的仓库和镜像名，方便追踪和管理。此外，可以同时为镜像设置标签（不指定默认为 ``latest`` ），如：

::

    $ docker build -t "datascience/web:v1" .

.. attention::

    上面构建命令的最后有个 ``.``，用于告诉 Docker 到本地目录中去找 Dockerfile。也可以通过 ``-f`` 选项指定 Dockerfile 位置（文件名可以不为 Dockerfile），如：

    ::

        $ docker build -t "datascience/web:v1"  -f path/to/dockerfile

.. note::

    由于每一步构建过程都将结果提交为镜像，所以可将之前的镜像层看做缓存。如本例如果新的应用只修改了 Dockerfile 的第 4 步，则构建时直接从第 4 步开始。需要忽略缓存构建可使用选项 ``--no-cache``：

    ::

        $ docker build --no-cache -t "datascience/web" .

    以上的镜像名称的引号可以省略。

查看新镜像
================

::

    $ docker images

如果想了解镜像构建的流程，可使用 ``docker history``，可以看到镜像的每一层和创建该层的指令。

从新镜像启动容器
======================

::

    $ docker run -d -p 80:80 --name web datascience/web nginx -g "daemon off;"

``docker run`` 的使用方法参见：:ref:`Docker 入门教程 <docker-tutorial>` 。

Dockerfile 指令
===================

除了上面已经说明的 ``RUN`` 和 ``EXPOSE``，Dockerfile 指令还有：

CMD
--------

``CMD`` 用于指定一个容器启动时要运行的命令。

::

    CMD ["/bin/bash", "-l"]

类似于 ``RUN``，区别在于：

- ``RUN`` 是指定镜像构建时要运行的命令
- ``CMD`` 指定容器启动时要运行的命令

.. note::

    使用 ``docker run`` 命令可以覆盖 ``CMD`` 指令。

.. attention::

    在 Dockerfile 中只能指定一条 ``CMD`` 指令，多条只有最后一条生效。

ENTRYPOINT
--------------

``ENTRYPOINT`` 用于执行指令，和 ``CMD`` 非常类似，但 ``ENTRYPOINT`` 提供的命令不会在容器启动时被覆盖！实际上，``docker run`` 指定的任何参数都会被当做参数传递给 ``ENTRYPOINT`` 指令中指定的命令。

::

    ENTRYPOINT ["/usr/sbin/nginx", "-g", "daemon off;"]

可以组合使用 ``ENTRYPOINT`` 和 ``CMD`` 来完成一些巧妙的工作（如果启动容器时没有指定参数，则在 ``CMD`` 中指定的 ``-h`` 会被传递给 ``Nginx``）：

::

    ENTRYPOINT ["/usr/sbin/nginx"]
    CMD ["-h"]

.. note::

    ``docker run`` 选项 ``--entrypoint`` 可以覆盖 ``ENTRYPOINT`` 指令。

WORKDIR
-------------

``WORKDIR`` 创建新容器时，在容器内部设置一个工作目录，``ENTRYPOINT`` 或 ``CMD`` 指定的程序会在这个目录下执行。

::

    WORKDIR /opt/webapp/db
    RUN bundle install
    WORKDIR /opt/webapp
    ENTRYPOINT ["rackuo"]

.. note::

    ``docker run`` 选项 ``-w`` 可在运行时覆盖工作目录。

ENV
---------

``ENV`` 用于在镜像构建过程中设置环境变量，环境变量被持久保存在镜像创建的任何容器中，可以在后续任何 ``RUN`` 指令中使用。

::

    ENV TARGET_DIR /opt/app
    ENV RVM_PATH=/home/rvm RVM_ARCHFLAGS="-arch_i486"
    WORKDIR $TARGET_DIR

.. note::

    ``docker run`` 选项 ``-e`` 可以运行时传递环境变量，但只会在运行时有效。

USER
-------

``USER`` 用于指定镜像以什么用户去运行，如果不指定默认为 root。

::

    USER user
    USER user:group
    USER uid
    USER uid:gid
    USER user:gid
    USER uid:group

.. note::

    ``docker run`` 选项 ``-u`` 可在运行时覆盖 ``USER`` 。

VOLUME
---------

``VOLUME`` 用来向基于镜像创建的容器添加卷（挂载点）。一个卷可以存在于一个或多个容器内特定的目录，这个目录可以绕过联合文件系统，用于数据共享或数据持久化。

卷功能让我们可以将数据（如源码）、数据库或者其他内容挂载到镜像中，而不是提交到镜像中，并且运行在多个容器间共享这些内容。

::

    VOLUME ["/opt/code", "/data"]

.. note::

    ``docker run`` 选项 ``-v`` 用于在运行时挂载本地目录到容器内挂载点。

    ``docker cp`` 允许从容器复制文件和复制文件到容器上。

ADD
--------

``ADD`` 用来将构建环境下的文件和目录复制到镜像中，不能对构建目录（指构建目录本身，但内部目录可以）或上下文之外的文件进行 ``ADD`` 操作。

::

    ADD software.lic /opt/application/software.lic

.. note::

    ``ADD`` 处理 gzip、bzip2、xz 时会自动解压。

    如果目的位置不存在，Docker 会自动创建全路径。

COPY
----------

``COPY`` 类似 ``ADD``，但不会对文件解压。同样如果目的位置不存在会自动创建。

::

    COPY conf.d/ /etc/apache2/

LABEL
---------

``LABEL`` 用于为镜像添加元数据，元数据以键值对展现。

::

    LABEL version="1.0"
    LABEL location="New York" type="Data Center"

.. tip::

    ``docker inspect`` 可以查看镜像中的标签信息。

ARG
---------

``ARG`` 用来定义可以在 ``docker build`` 运行时传递的变量。只需要在构建时使用 ``--build-arg``，但只能指定 Dockerfile 中定义过的参数。

::

    ARG build
    ARG webapp_user=user

    $ docker build --build-arg build=1234 -t datascience/web .

Docker 预定义的 ``ARG`` 变量：``HTTP_PROXY``, ``http_proxy``, ``HTTPS_PROXY``, ``https_proxy``, ``FTP_PROXY``, ``ftp_proxy``, ``NO_PROXY``, ``no_proxy`` 。

STOPSIGNAL
-------------

``STOPSIGNAL`` 用来设置停止容器时发送什么系统调用信号给容器，如 ``SIGKILL`` 。

ONBUILD
------------

``ONBUILD`` 为镜像添加触发器，详情请查阅 `Dockerfile reference`_ 。

.. _Dockerfile reference: https://docs.docker.com/engine/reference/builder/











