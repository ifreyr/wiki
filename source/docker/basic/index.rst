.. _docker-basic:

====================
Docker 基础
====================

.. toctree::
    :caption: Docker
    :hidden:

    install-docker
    install-docker-compose
    run-docker-no-root
    docker-tcp
    docker-tutorial
    dockerfile-tutorial
    docker-compose-tutorial



:doc:`install-docker`
    以 CentOS 7 为例，介绍 Docker 安装。

:doc:`install-docker-compose`
    Docker 编配工具 docker-compose 安装。

:doc:`run-docker-no-root`
    Docker 默认只能在 root 下使用，添加用户到 docker 用户组实现非 root 账号使用。

:doc:`docker-tcp`
    开启 Docker 远程访问。

:doc:`docker-tutorial`
    Docker 基本使用，了解容器如何启动、查看、停止和删除。

:doc:`dockerfile-tutorial`
    Dockerfile 指令和使用 Dockerfile 构建镜像。

:doc:`docker-compose-tutorial`
    Docker 基础编配工具 Docker-compose 的语法和命令。

