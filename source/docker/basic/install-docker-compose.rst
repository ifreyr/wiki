.. _install-docker-compose:

=======================
Docker-compose 安装
=======================

方法一：pip 安装
=======================

::

    $ pip install docker-compose


方法二：下载安装
=======================

下载：

    $ sudo curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose


添加执行权限：

    $ sudo chmod +x /usr/local/bin/docker-compose


查看版本
============

::

    $ docker-compose --version

    docker-compose version 1.16.1, build 6d1ac21
    docker-py version: 2.5.1
    CPython version: 2.7.13
    OpenSSL version: OpenSSL 1.0.1t  3 May 2016






