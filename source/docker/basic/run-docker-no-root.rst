.. _run-docker-no-root:

===========================
非 root 用户使用 Docker
===========================


创建 docker 用户组：

::

    $ sudo groupadd docker

当前用户加入 docker 组：

::

    $ sudo gpasswd -a ${USER} docker

重启 docker 服务：

::

    $ sudo systemctl daemon-reload
    $ sudo systemctl restart docker

运行 docker 验证：

::

    $ docker ps